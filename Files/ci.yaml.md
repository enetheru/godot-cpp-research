The existing solution builds for a subset of configurations and only tests for one.

Build Configurations are as follows

🐧 Linux (GCC)
🐧 Linux (GCC, Double Precision)
🏁 Windows (x86_64, MSVC)
🏁 Windows (x86_64, MinGW)
🍎 macOS (universal)
🤖 Android (arm64)
🍏 iOS (arm64)
🌐 Web (wasm32)
🐧 Build (Linux, GCC, CMake)
🐧 Build (Linux, GCC, CMake Ninja)
🏁 Build (Windows, MSVC, CMake)

Of which only Linux(GCC) is tested.