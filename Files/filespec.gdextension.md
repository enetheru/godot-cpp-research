Section `[configuration]`
Required
`entry_symbol = "entry_symbol"`
`compatibility_minimum = "x[.x[.x]]"`
Optional
`compatibility_maximum = "x[.x[.x]]"`
`reloadable = "<bool>"`
`autodetect_library_prefix =  "<path>"`

Section `[libraries]`

Section `[icons]`

```
[configuration]

entry_symbol = "example_library_init"
compatibility_minimum = "4.1.0"
compatibility_maximum = "4.2.2"
reloadable = "true"
autodetect_library_prefix = "/path/to/extension/libs"

[libraries]

macos.debug = "res://bin/libgdexample.macos.template_debug.framework"
macos.release = "C:/bin/libgdexample.macos.template_release.framework"
windows.debug.x86_32 = "bin/libgdexample.windows.template_debug.x86_32.dll"

[icons]
iconkey = <path>
```
