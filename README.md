There are [[People Involved]]

There are a number of things to achieve
* [[Development Platforms]]
* [[Dependency Chain]]
- [[Compile Configuration]]
	- [[Pre-sets]] - standard sets of options for the below four
	- [[Toolchains]] - target platform/arch
	- [[Compile Definitions ]]- defines that change the meaning of code
	- [[Compile Options]] - flags to alter the behaviour of the compiler
	- [[Link Options]] - flags to alter the behaviour of the linker
- [[Compile Process]]
	- api dump
	- godot-cpp
	- gdextension-template
- Tests
- [[Continuous Integration]]
- Linting
- Packaging

## Godot-CPP-Template
There is a need to keep the template up-to date too otherwise one without the other seems silly.

it ends up being a bit of a weird situation where the parent folder becomes the library consumed by the sub folder.

The godot-test folder needs to be able to be run as part of github actions

Which makes me think is there a way to test github actions offline?

### Defaults godot-cpp
What are the current defaults?

What should the defaults be, and why?

IMHO, the defaults should be to build a valid release library, and valid release editor extension.

The default target platform should be the host platform

### Defaults godot-cpp-template
The default configuration should match the default configuration of the godot-cpp library
Additional care should be given to github workflows, to add a full compliment of exensions for all target platforms and uploaded to a github workspace for download and testing
and a release action to take some cached build and make it available for download.

