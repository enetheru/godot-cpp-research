Compile options is synonymous with compile flags in that they tell the compiler what to do.
But they exclude link options and definitions as they are handled by other things.

For instance, optimisation levels, debug options, compiler features, etc all controlled with compiler options.
Which means that each compiler has a different set, though some may share.

That means we need to be able to differentiate between compilers
Ones I know off the top of my head are:
msvc
gcc
clang
apple clang
other clang variants
intel

I guess each of these might have to be split into their own document describing the features we use and why, but well, there it is.

I think sometimes these also need to be split up depending on the platform, gcc on windows using mingw might have some special use cases.

The way it's currently split is by platform, I guess that's the way it will be.

## Options and what they do


### `-Wwrite-strings`

https://gcc.gnu.org/onlinedocs/gcc/Warning-Options.html#index-Wwrite-strings

When compiling C, give string constants the type `const char[length]` so that copying the address of one into a non-`const` `char *` pointer produces a warning. These warnings help you find at compile time code that can try to write into a string constant, but only if you have been very careful about using `const` in declarations and prototypes. Otherwise, it is just a nuisance. This is why we did not make -Wall request these warnings.

When compiling C++, warn about the deprecated conversion from string literals to `char *`. This warning is enabled by default for C++ programs.

This warning is upgraded to an error by -pedantic-errors in C++11 mode or later.

### `-fvisibility=hidden`
https://gcc.gnu.org/wiki/Visibility
https://labjack.com/blogs/news/simple-c-symbol-visibility-demo