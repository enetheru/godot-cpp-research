Link options I don't know much about, other than that I can specify a different linker like mold

#### `-R '$$ORIGIN'`
with rpath origin, is used in linux to allow the dynamic linker to search in directories next to the library. Is not relevant for windows, but is for linux and macos.
If your extension also relied on loading shared libs this would allow them to simply be placed next to the extension.

In CMAKE there are specific options for this

https://stackoverflow.com/questions/57915564/cmake-how-to-set-rpath-to-origin-with-cmake
https://cmake.org/cmake/help/latest/prop_tgt/BUILD_RPATH_USE_ORIGIN.html

Something that might be useful at some point:
https://cmake.org/cmake/help/latest/manual/cmake-generator-expressions.7.html#genex:TARGET_RUNTIME_DLLS

#### `-no-undefined`
https://flameeyes.blog/2008/11/19/relationship-between-as-needed-and-no-undefined-part-1-what-do-they-do/