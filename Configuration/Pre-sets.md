There are three known pre-sets I am aware of

Release Template
Release Debug
and Editor

But they only manipulate  a very small subset of the options specifically for Godot use, not really Godot development.

There are additional options which may be used as presets. and i can see choosing he target operating system too.

So perhaps presets should be listed more like the actual targets that will be desired. like:
`libgodot-cpp[.<precision>][.editor][.<dev>].<platform>.<architecture>[.build_type]`
it really depend on the options provided. I know there were presets for the orchestrator and other template projects made by vorlac

---

Vorlac's presets much like the ones builtin to clion list the compiler and the build type as part of the target.

the presets in the template project really jut are compielr and build type aka debug/release and variants.

---

The orchestrator plugin does a lot more so lets look at that.

It also mainly lists compiler and build type, but it also does something interesting with conditions on the profile, so it wont expose linux based build types to windows, just the msvc, which is a mistake for someone who has mingw.. but I can see how it can be interesting to have.

