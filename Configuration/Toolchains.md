I need to generate toolchain files for a set of compilers

Scons list the following options

architectures:
arm32
arm64
ppc32
ppc64
rv64
universal
wasm32
x86_32
x86_64

with aliases below. These can just be tested against, no need to have aliases:

"x86": "x86_32",  
"x64": "x86_64",  
"amd64": "x86_64",  
"armv7": "arm32",  
"armv8": "arm64",  
"arm64v8": "arm64",  
"aarch64": "arm64",  
"rv": "rv64",  
"riscv": "rv64",  
"riscv64": "rv64",  
"ppcle": "ppc32",  
"ppc": "ppc32",  
"ppc64le": "ppc64",

Platforms as listed in Godot source under platforms

- android  
- ios  
- linuxbsd
- macos  
- web  
- windows  

---


ughuuu has submitted a pull request for godot-cpp-template which adds a bunch of github actions which may provide the basis for this.

these are the variables present. but not all combinations hold.

and the scons files list alternates I think.

arch
- arm32  
- arm64  
- universal  
- wasm32  
- x86_32  
- x86_64  

float-precision
- double   
- single  

github os runners
- macos-latest    
- ubuntu-20.04  
- windows-latest  

---

I know that there was an attempt by ivanTheInventor for adding toolchains into his pull request. These toolchains appear to be externally supplied by android sdk, and emscripten projects.

which I guess makes sense. I will have to look at other projects for toolchains and where they exists or are built from.

the ughuu godot-cpp template has a bunch of options specified for building for ios
https://github.com/Ughuuu/godot-cpp-template/tree/add-more-stuff/options-to-build
