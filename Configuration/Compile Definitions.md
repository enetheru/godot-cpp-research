I have first hand experience with setting compile defitions, its a lot better to use the target compile definitions than it is to set cxx flags directly.

That way we can also use generator expressions to properly consider the nature of the os and such.

The build flags I have off the top of my head are

TOOLS_ENABLED
DEV_BUILD
FLOAT_PRECISION
HOT_RELOAD_ENABLED
DEBUG_ENABLED  
DEBUG_METHODS_ENABLED
TYPED_METHOD_BIND