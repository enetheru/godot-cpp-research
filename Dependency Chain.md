The dependency chain is actually very simple to grasp, but complex in practice due the variety of variants for the Godot program itself.

```
GDExtension -> godot-cpp -> Godot
Where Godot can be any of variant of [Platform] * [Version] * [Features]
``` 

There are 6 platforms that are regularly built for testing. Of the 4 series there is 4.1 4.2, and regularly changed features like double precision. so right away we have 24 variants.

![[Dependency Chain.canvas]]
The way Godot loads up the library is checked against the version and feature tags listed in the [[filespec.gdextension|gdextension specification]] file.

"GDExtension add-ons compiled for a given Godot version are only guaranteed to work with the same minor release series. For example, a GDExtension add-on compiled for Godot 4.0 will only work with Godot 4.0, 4.0.1, 4.0.2. In addition, GDExtension is not compatible with Godot 3.x.

GDExtension add-ons are also only compatible with engine builds that use the level of floating-point precision the extension was compiled for. This means that if you use a engine build with double-precision floats, the extension must also be compiled for double-precision floats. See [Large world coordinates](https://docs.godotengine.org/en/stable/tutorials/physics/large_world_coordinates.html#doc-large-world-coordinates) for details." - [Godot Documentation](https://docs.godotengine.org/en/stable/tutorials/scripting/gdextension/what_is_gdextension.html)

it appears to be in gdextension::load_extension_resource that there are checks to names.

First we hit the compatibility minimum
```cpp
if (compatibility_minimum[0] < 4 || (compatibility_minimum[0] == 4 && compatibility_minimum[1] == 0)) {  
    ERR_PRINT(vformat("GDExtension's compatibility_minimum (%d.%d.%d) must be at least 4.1.0: %s", compatibility_minimum[0], compatibility_minimum[1], compatibility_minimum[2], p_path));  
    return ERR_INVALID_DATA;  
}  
  
bool compatible = true;  
// Check version lexicographically.  
if (VERSION_MAJOR != compatibility_minimum[0]) {  
    compatible = VERSION_MAJOR > compatibility_minimum[0];  
} else if (VERSION_MINOR != compatibility_minimum[1]) {  
    compatible = VERSION_MINOR > compatibility_minimum[1];  
} else {  
    compatible = VERSION_PATCH >= compatibility_minimum[2];  
}  
if (!compatible) {  
    ERR_PRINT(vformat("GDExtension only compatible with Godot version %d.%d.%d or later: %s", compatibility_minimum[0], compatibility_minimum[1], compatibility_minimum[2], p_path));  
    return ERR_INVALID_DATA;  
}
```

Then it tries to get the library path with the following:
```cpp
String library_path = GDExtension::find_extension_library(p_path, config, [](String p_feature) { return OS::get_singleton()->has_feature(p_feature); });
```

what is that, I have to look back up to get the meaning.
`p_path` is the path to the configuration file, so `my_extension.gdextension`
config is a `Ref<ConfigFile>` that has loaded the config file.
and then it has a lambda that returns a bool. I have to look into the `find_extension_library`

First we get all the library keys
split it into tags using '.' as a separator
then we call `if( p_has_feature(tag) )`
given we pass, then we test the best match for file existence.

So I need to look at p_has_feature, wait thats the lambda we passed in before.

so really its `OS::get_singleton()->has_feature(p_feature);`

This makes so much more sense now, what a weird little thing.

Compile Definitions that effect the feature flags
Some of these are really important, but others can be omitted.
For instance, debug isn't really needed is it, perhaps it might cause a problem I guess it needs to be tested.

example combinations
debug.editor.x86_64.64.double.

```cpp
bool OS::has_feature(const String &p_feature) {  
    // Feature tags are always lowercase for consistency.  
    if (p_feature == get_identifier()) {  
       return true;  
    }  
  
    if (p_feature == "movie") {  
       return _writing_movie;  
    }  
  
#ifdef DEBUG_ENABLED  
    if (p_feature == "debug") {  
       return true;  
    }  
#endif // DEBUG_ENABLED  
  
#ifdef TOOLS_ENABLED  
    if (p_feature == "editor") {  
       return true;  
    }  
#else  
    if (p_feature == "template") {  
       return true;  
    }  
#ifdef DEBUG_ENABLED  
    if (p_feature == "template_debug") {  
       return true;  
    }  
#else  
    if (p_feature == "template_release" || p_feature == "release") {  
       return true;  
    }  
#endif // DEBUG_ENABLED  
#endif // TOOLS_ENABLED  
  
#ifdef REAL_T_IS_DOUBLE  
    if (p_feature == "double") {  
       return true;  
    }  
#else  
    if (p_feature == "single") {  
       return true;  
    }  
#endif // REAL_T_IS_DOUBLE  
  
    if (sizeof(void *) == 8 && p_feature == "64") {  
       return true;  
    }  
    if (sizeof(void *) == 4 && p_feature == "32") {  
       return true;  
    }  
#if defined(__x86_64) || defined(__x86_64__) || defined(__amd64__) || defined(__i386) || defined(__i386__) || defined(_M_IX86) || defined(_M_X64)  
#if defined(__x86_64) || defined(__x86_64__) || defined(__amd64__) || defined(_M_X64)  
    if (p_feature == "x86_64") {  
       return true;  
    }  
#elif defined(__i386) || defined(__i386__) || defined(_M_IX86)  
    if (p_feature == "x86_32") {  
       return true;  
    }  
#endif  
    if (p_feature == "x86") {  
       return true;  
    }  
#elif defined(__arm__) || defined(__aarch64__) || defined(_M_ARM) || defined(_M_ARM64)  
#if defined(__aarch64__) || defined(_M_ARM64)  
    if (p_feature == "arm64") {  
       return true;  
    }  
#elif defined(__arm__) || defined(_M_ARM)  
    if (p_feature == "arm32") {  
       return true;  
    }  
#endif  
#if defined(__ARM_ARCH_7A__)  
    if (p_feature == "armv7a" || p_feature == "armv7") {  
       return true;  
    }  
#endif  
#if defined(__ARM_ARCH_7S__)  
    if (p_feature == "armv7s" || p_feature == "armv7") {  
       return true;  
    }  
#endif  
    if (p_feature == "arm") {  
       return true;  
    }  
#elif defined(__riscv)  
#if __riscv_xlen == 8  
    if (p_feature == "rv64") {  
       return true;  
    }  
#endif  
    if (p_feature == "riscv") {  
       return true;  
    }  
#elif defined(__powerpc__)  
#if defined(__powerpc64__)  
    if (p_feature == "ppc64") {  
       return true;  
    }  
#endif  
    if (p_feature == "ppc") {  
       return true;  
    }  
#elif defined(__wasm__)  
#if defined(__wasm64__)  
    if (p_feature == "wasm64") {  
       return true;  
    }  
#elif defined(__wasm32__)  
    if (p_feature == "wasm32") {  
       return true;  
    }  
#endif  
    if (p_feature == "wasm") {  
       return true;  
    }  
#endif  
  
#if defined(IOS_SIMULATOR)  
    if (p_feature == "simulator") {  
       return true;  
    }  
#endif  
  
    if (_check_internal_feature_support(p_feature)) {  
       return true;  
    }  
  
    if (has_server_feature_callback && has_server_feature_callback(p_feature)) {  
       return true;  
    }  
  
    if (ProjectSettings::get_singleton()->has_custom_feature(p_feature)) {  
       return true;  
    }  
  
    return false;  
}
```

But since the feature is a best fit scenario, its more like a constraint.

