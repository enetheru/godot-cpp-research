Ivan's CMake Build
==================
I have successfully built a working library using ivans remake.

godot-cpp
---------

CONFIGS_WITH_DEBUG                  = Debug;RelWithDebInfo
CMAKE_BUILD_TYPE                    = Debug
TARGET                              = TEMPLATE_DEBUG
DEFAULT_PLATFORM                    = WINDOWS
PLATFORM                            = WINDOWS
GDEXTENSION_DIR                     = gdextension
GEXTENSION_API_FILE                 = gdextension/extension_api.json
FLOAT_PRECISION                     = single
OPTIMIZE                            = AUTO
SYMBOL_VISIBILITY                   = AUTO
GODOT_CPP_LIBRARY_TYPE              = STATIC
DEV_BUILD                           = OFF
DEBUG_SYMBOLS                       = OFF
USE_HOT_RELOAD                      = ON
GODOT_DISABLE_EXCEPTIONS            = ON
GENERATE_TEMPLATE_GET_NODE          = ON

DEBUG_SYMBOLS_ENABLED               = ON

target_compiler_definitions
    DEBUG_ENABLED
    DEBUG_METHODS_ENABLED
    GDEXTENSION
    HOT_RELOAD_ENABLED
    NDEBUG
    WINDOWS_ENABLED

target_compile_options
    -O2
    -Og
    -fno-exceptions
    -fvisibility=hidden
    -g2
    -gdwarf-4

target_link_options
    -Wl,--no-undefined
    -fvisibility=hidden
    -s
    -static
    -static-libgcc
    -static-libstdc++

