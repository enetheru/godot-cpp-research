Orlac's Template is kinda nice in that it also provides you a way to build and debug the engine along with the source. Lets step through the cmake script and see whats up.

First couple of observations is that it pulls the engine as a submodule
it uses fetchcontent to fetch the godot-cpp library
and it uses presets

it sets global c++ standard and -fPIC

It then pulls the godot-cpp git repo using fetchcontent using a tag.
I'm assuming that the tag lines up with the godot submodule

it globs the source code from the src folder, then adds the shared library.

oh and it target links the fetchcontent based gdextension.

so other than PIC, it doesn't do much. very simple.

I wonder if it works.
not out of the box with newer things.

I didn't think it would considering that it is a rather simple configuration and relies on the godot-cpp project being setup correctly.