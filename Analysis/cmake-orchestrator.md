
Uses cmake presets
uses submodules for both godot-cpp and the godot-engine

OK, first up it sets TOOLS_ENABLED globally
same as CXX_STANDARD to 20, and some other flags.
This isn't a project used to build other projects

we add both our own cmake dir to modules, and the extern/godot-cpp/cmake dir.

Then we include some modules.

generate-authors - internal
generate-licence - internal
generate-version - internal
generate-donors - internal
godot-extension-db-generator - Interesting
We use a copy of the generator python script rather than the one from the godot-cpp repository
and then do some fancy shit with their own files. I think probably extending the godot extension libs with their own things.

and immediaely run the functions defined in those modules, they could just have the run statement at the bottom of the module but meh. style issues aren't real issues.

if APPLE we set a cache string marking architectures that apple uses as x86_64 and arm64

Then we have the project command

we re-use the compiler_is_xx from the godot-cpp source even though its a bad idea.

include
godot-dev-configuration
{
We pull/update the engine sources 
and the godot-cpp sources
There is commented out code to build the engine
we add the godot-cpp as a subdir
then commented out code to add the engine sources so that it gets picked up in IDE's
}

add the sources

define the library 

then we target_compile_options with generator expressions



We also setup ccache

add includes

add some more link options

i'll skip the clang-format stuff for now.

add godot::cpp as a library to link to

Then there is some configuration of cmake/windows.rc.in

and finally properties are set on the library

include cmake-utils
and print project variables().

Lets go back over it again and look into the sub files.