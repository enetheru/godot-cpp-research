CMake Build
===========
This is the build profile of the default cmake scripts as they exist, of which
I cannot produce a valid binary from for use with the default compile of godot.

godot-cpp
---------

GENERATE_TEMPLATE_GET_NODE      = ON
GODOT_CPP_SYSTEM_HEADERS        = ON
GODOT_CPP_WARNING_AS_ERROR      = OFF

compiler_is_clang               = FALSE
compiler_is_gnu                 = TRUE
compiler_is_msvc                = FALSE

CMAKE_BUILD_TYPE                = Debug
BITS                            = 64

GODOT_GDEXTENSION_DIR           = "gdextension"
GODOT_CUSTOM_API_FILE           = ""
GODOT_GDEXTENSION_API_FILE      = gdextension/extension_api.json

FLOAT_PRECISION                 = single
GODOT_COMPILE_FLAGS             =
GODOT_COMPILE_FLAGS            += -fno-omit-frame-pointer -O0 -g

GODOT_DISABLE_EXEPTIONS         = ON
GODOT_COMPILER_FLAGS           += -fno-exceptions

Find python

GENERATE_BINDING_PARAMETERS     = TRUE

GENERATED_FILES_LIST            = generated list of source files

generate bindings

Collect SOURCES
collect HEADERS

add static library using SOURCES, HEADERS, GENERATED_FILES_LIST
add library alias

include GodotCompilerWarnings

compiler_less_than_v8
target_compile_options
these are for clang or gnu
``` 
    -Wall
    -Wctor-dtor-privacy
    -Wextra
    -Wno-unused-parameter
    -Wnon-virtual-dtor
    -Wwrite-strings
```

these are for just gnu
```
    -Walloc-zero
    -Wduplicated-branches
    -Wduplicated-cond
    -Wno-misleading-indentation
    -Wplacement-new=1
    -Wshadow-local
    -Wstringop-overflow=4
```
my compiler is gcc v13+
```
    -Wno-return-type
```
target_compile_features
    cxx_std_17

target_compile_definitions
    DEBUG_ENABLED
    DEBUG_METHODS_ENABLED

target_link_options
    -static-libgcc
    -static-libstdc++
    -Wl,R,'$$ORIGIN'

GODOT_CPP_SYSTEM_HEADERS_ATTRIBUTE  = SYSTEM

target_include_directories

set_property on target
    COMPILE_FLAGS                   = GODOT_COMPILE_FLAGS

set target properties with target
    CXX_EXTENSIONS                  = OFF
    POSITION_INDEPENDENT_CODE       = ON

and some other superfluous things

### Stripped down version

CMAKE_BUILD_TYPE                = Debug

target_compile_features
    cxx_std_17

target_compile_options
    -fno-omit-frame-pointer
    -O0
    -g
    -fno-exceptions


target_compile_definitions
    DEBUG_ENABLED
    DEBUG_METHODS_ENABLED

target_link_options
    -static-libgcc
    -static-libstdc++
    -Wl,R,'$$ORIGIN'


set target properties with target
    CXX_EXTENSIONS                  = OFF
    POSITION_INDEPENDENT_CODE       = ON

godot-cpp/test
--------------

ahh crap.

GODOT_GDEXTENSION_DIR               = ../gdextension
CPP_BINDINGS_PATH                   = ../
TARGET_PATH                         = win64

we set a bunch of paths that are irrelevant

CMAKE_CXX_STANDARD                  = 17
CMAKE_CXX_STANDARD_REQUIRED         = ON
CMAKE_CXX_EXTENSIONS                = OFF

GODOT_COMPILE_FLAGS                 = ""
GODOT_LINK_FLAGS                    = ""

GODOT_LINKER_FLAGS                 += -static-libgcc -static-libstdc++ -Wl,-R,'$$ORIGIN' 

GODOT_COMPILE_FLAGS                += -fPIC -g -Wwrite-strings 

GODOT_COMPILE_FLAGS                += -fno-omit-frame-pointer -O0
GODOT_COMPILE_FLAGS                += -fno-exceptions 

# stripped down

CMAKE_CXX_STANDARD                  = 17
CMAKE_CXX_STANDARD_REQUIRED         = ON
CMAKE_CXX_EXTENSIONS                = OFF

target_compile_options:
    -fPIC
    -g
    -Wwrite-strings 
    -fno-omit-frame-pointer
    -O0
    -fno-exceptions 

target_link_options:
    -static-libgcc
    -static-libstdc++
    -Wl,-R,'$$ORIGIN' 
