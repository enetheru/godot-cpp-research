Scons Build
===========
Build Profile of scons, using default behaviour and no flags.
This produces valid binararies for a default compile of godot editor.

Using the 4.2 branch of godot
Using the 4.2 branch of godot-cpp

godot-cpp
---------
call to cpp_tool aka tools/godot-cpp.py.options(...)

set platform            = windows
set target              = template_debug
set gdextension_dir     = None
set custom_api_file     = None
set generate_binding    = False
set generate_template_get_node  = True
set build_library       = True
set precision           = single
set arch                = x86_64
set compiledb           = False
et compiledb_file       = compile_commands.json
set use_hot_reload      = None
set disable_exceptions  = True
set symbols_visibility  = hidden

call to platform.options(opts), in our case windows.py

set use_mingw           = False
set use_clang_cl        = False
set use_static_cpp      = True

end windows.py.options(opts)
call to targets.py.options(opts)

set optimise            = speed_trace
set debug_symbols       = True
set dev_build           = False

end targets.py.options(opts)
end godotcpp.py.options(opts)
Call to cpp_tool.generate(...)

set num_jobs            = cpu_count - 1
set arch                = x86_64

add CPPDEFINES          HOT_RELOAD_ENABLED

call to windows.py.generate()

set use_mingw           = True

call to mingw.generate()
# TODO Is this inbuilt to scons?
end call to mingw.generate()

add CCFLAGS             -Wwrite-strings
add LINKFLAGS           -Wl,--no-undefined
add LINKFLAGS           -static -static-libgcc -static-libstdc++

add CPPDEFINES          WINDOWS_ENABLED

end call to windows.py.generate()
call to targets.py.generate()

set editor_build        = False
set dev_build           = False
set debug_features      = True
set opt_level           = speed_trace
set debug_symbols       = False

add CPPDEFINES          DEBUG_ENABLED
add CPPDEFINES          DEBUG_METHODS_ENABLED
add CPPDEFINES          NDEBUG

add LINKFLAGS           -s
add CCFLAGS             -O2

end call to targets.py.generate()

add CXXFLAG             -fno-exceptions

add CCGLAGS             -fvisibility=hidden
add LINKFLAGS           -fvisibility=hidden

add CXXFLAGS            -std=c++17

add CPPDEFINES          GDEXTENSION

make GodotCPP() godotcpp.py._godot_cpp()

call to env.GodotCPP()

Generate Bindings
add sources
compile static lib

### Cut down version

target_compile_definitions:
	DEBUG_ENABLED
	DEBUG_METHODS_ENABLED
	GDEXTENSION
	HOT_RELOAD_ENABLED
	NDEBUG
	WINDOWS_ENABLED

target_compile_options
	-O2
	-fno-exceptions
	-fvisibility=hidden
	-std=c++17

target_link_flags
    --no-undefined
    -s
    -static
    -static-libgcc
    -static-libstdc++

godot-cpp/test
--------------

first, we pull in the parent environment, pretty sure that means I pull in all
those options.

then we just add our sources to the list.
and build a shared library using those details.

not much to it.
