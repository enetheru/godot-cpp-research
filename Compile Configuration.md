
- [[Pre-sets]] - standard sets of options for the below four
- [[Toolchains]] - target platform/arch
- [[Compile Definitions ]]- overrides
- [[Compile Options]] - overrides
- [[Link Options]] - overrides

## Option Scope
CMAKE allows to specify the scope of the option in regards to its ability to propagate across dependent targets.

PRIVATE are only used in the target they are set for
INTERFACE are only exposed to the dependent target
PUBLIC are exposed to both

So when specifying options in the other sections they need a scope and the reason why.
## Platforms
Configuration needs to be split per platform to match the nature of the scons scripts, so that people who work with scons have an easier time evaluating  changes to cmake.

The existing scons scripts are located in a folder called 'tools' and have a file per platform, and then a couple of helper and generic files

These scripts specify the options that are exposed to the help command per platform, but all of them are sourced at configure time.

Platforms:

- [[Android]]
- [[IOS]]
- [[Linux]]
- [[MacOS]]
- [[Web]]
- [[Windows]]
