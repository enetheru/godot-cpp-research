### Imports

As part of the scons script it imports some SCons.Tool scripts called clang and clangxx and common_compiler_flags script. which are tacked onto the end of the script.

The clang import appears to be simply a toolchain, so that will have to be handled separately in the toolchain configuration.
### Configure Options
it adds use_llvm, but that doesnt make sense in a cmake context, rather the use of toolchain files does.

### Compile Definitions
`LINUX_ENABLED`
`UNIX_ENABLED`
### Compile Options
if not llvm/clang
	if `HOT_RELOAD_ENABLED` add `-fno-gnu-unique`

`-fPIC`
`-Wwrite-strings`

Then it goes onto specify architecture flags
match arch
`x86_64`:
	`-m64`
	`-march=x86-64`
`x86_32`:
	`-m32`
	`-march=x86-32`
`arm64`:
	`-march=armv8-a`
`rv64`:
	`-march=rv64gc`
### Link Options
`-Wl,-R,'$$ORIGIN'`

And then architecture flags
`x86_64`:
	`-m64`
	`-march=x86-64`
`x86_32`:
	`-m32`
	`-march=x86-32`
`arm64`:
	`-march=armv8-a`
`rv64`:
	`-march=rv64gc`