An example of similar initiatives would bet the VFX Reference Platform

It's probably  good idea to mirror the platforms that github use for runners. and the ones that are used for both godot and godot-cpp.

AFAIK these are:
- windows-2019
- ubuntu-20.04
- macos-11

Software used:
	CMake minimum version, I think 3.15 is a good starting point, because ubuntu-20.04 has that.

somehow mingw needs to come into play
also windows subsystem for linux aka WSL

Compilers
- msvc
- gcc
- Clang
- AppleClang

IDE's
* clion
* msvc
* vscode
* qt creator
* kdevelop
* Terminal only with things like vim/emacs