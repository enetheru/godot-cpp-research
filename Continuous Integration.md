The primary method of CI is by using GitHub Actions.
Unfortunately running github actions locally is not easy, I only know of 'act' which uses docker to try to simulate what github has. 
Github Actions to run all combinations of platform/arch/target
## GitHub Actions
### Actions
#### Publish
#TODO Take specific build artifacts and publish them as a release.
#### Clean Action Runs
#TODO an action to clean out old runs
https://github.com/marketplace/actions/delete-workflow-runs
#### Clean Cache
#TODO an action to clean out old cached items.
https://stackoverflow.com/questions/63521430/clear-cache-in-github-actions
### Workflow
#### push/pull request
This is the action that runs on every commit to ensure compatibility
Compiles for all targets.

### Schedule
#### Update API
I think a regular schedule to update the api files would be nice.

## Notes
I'm having to figure out how the CI works for github so that future changes can be tested for regression.

There is a matrix of options, and it allows you to specify what the options are upfront before use.

Host Runner

I've been having trouble replicating the scons builds, and compiling the example using mingw and cmake.

So I started from scratch from the branch 4.2 which in my case is 4.2.2

Scons works no problems

When I attempted the following it didnt link due to missing library
```powershell
cd godot-cpp
cmake .
cmake --build .
cd test
cmake .
cmake -build .
```
`cannot find -lgodot-cpp.windows.release.64: No such file or directory`

The default build mode for cmake is Debug, so trying configuration
```ps
cd godot-cpp
cmake . -DCMAKE_BUILD_TYPE:STRING="Release"
cmake --build .
cd test
cmake .
cmake --build .
```
This builds the library libgdexample.dll

But it puts it in godot-cpp/test/bin/win64/gdexample.dll

Which has nothing to do with the project.
Lets copy it and see if it works.

I had to rebuild scons because the copy to a back up didnt work. I want to see if I replicate the exact thing it works.

what in the actual fuck.. now scons builds but it doesnt load in the editor.

the CI configuration looks like this:
```
cd test
scons target=template_release
```
So hopefully that is all the change needed for it to work.

wrong, My git tree was polluted with things from master. re-doing it.

Note: Thinking to myself about updatability, being able to pull a version and recompile and have a valid output. currently its super fragile

OK, so I couldnt get it to work, and then I did.. The issue I have is that I dont remember exactly what it was that made it work. the target=template_release I omitted from my last run.
and I am running godot with --headless

My Godot also doesn't quit
OK I have compile issues. the cmake project doesnt produce consumable binaries.
I think I might have to do another fresh clean build.

Github Actions

In order to test out the nature of the builds, a CI setup is required

Existing  scons builds are:
- Linux gcc
- linux gcc double
- windows msvc
- windows mingw
- macos universal
- android arm64
- ios arm64
- web wasm32
and cmake ones:
- linux gcc
- linux gcc ninja
- windows msvc

There is an ideal setup in here somewhere, and mostly targeted towards platforms.

If we can use the CI to prove compiling for various platforms that would be awesome too.

So Host System Linux
Targets
GCC, Clang

`<host>/<compiler>/<platform>/<arch>/<conf>`
Which creates a graph like:
```

windows-2019
	msvc
		windows
			[single, double]
		android
			arm32
			arm64
			...
	mingw-gcc
		windows
			[single, double]
		android
			arm32
			arm64
			...
		web
	mingw-clang
		windows
			[single, double]
		android
			arm32
			arm64
		web
ubuntu-20.04
	gcc
		linux
	clang
		linux
	mingw
		windows
macos-11
	?
```
```

I've been focusing on the mingw runner using msys2 mingw64, and something I discovered is that the default godot source tree with testing doesnt support out of tree builds.

Thats how broken the cmake implementation is.