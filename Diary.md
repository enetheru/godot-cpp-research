I have to start planning what I need to do otherwise I am going to forget or lose track of my progress towards the final goal.

First and foremost the original plan was to split out the multiplayer module so that it can be extended and messed with with indiscretion, but quickly I found that it was quite difficult to fully complete due to missing and weird data requirements. I spend two weeks porting and running into trouble. It taught me a lot about the structure of godot itself.

Next I was just messing with getting a good workflow to compile and build simply and reliably with existing modules, and then put in my code, but I was also running into trouble with that step because the scons solution makes little sense.

Then I was messing with cmake and seeing if i could extend the existing setup, which seemed untenable.

So I started my own from scratch.

And then I found other people had also started theirs from scratch

And there were plenty of pull requests, and re-writes from various other people.

After much deliberating, I am now at the point of wanting to consolidate what exists already.

I've spent a week or two struggling to bring people together and review the rewrite from ivan, and I have some tentative committment, but those who are involved also have other things to do.

So I Started pulling apart the solutions again.

Scons has this to say about the default configuration

"By default, **scons** searches for known programming tools on various systems and initializes itself based on what is found. On Windows systems which identify as _win32_, **scons** searches in order for the Microsoft Visual C++ tools, the MinGW tool chain, the Intel compiler tools, and the PharLap ETS compiler. On Windows system which identify as _cygwin_ (that is, if **scons** is invoked from a cygwin shell), the order changes to prefer the GCC toolchain over the MSVC tools. On OS/2 systems, **scons** searches in order for the OS/2 compiler, the GCC tool chain, and the Microsoft Visual C++ tools, On SGI IRIX, IBM AIX, Hewlett Packard HP-UX, and Oracle Solaris systems, **scons** searches for the native compiler tools (MIPSpro, Visual Age, aCC, and Forte tools respectively) and the GCC tool chain. On all other platforms, including POSIX (Linux and UNIX) platforms, **scons** searches in order for the GCC tool chain, and the Intel compiler tools. These default values may be overridden by appropriate setting of construction variables."
https://scons.org/doc/production/HTML/scons-man.html

re-interpreting this into something that is easier to read:

Search Lists in order:
On Windows systems which identify as _win32_:
	Microsoft Visual C++ tools
	The MinGW tool chain
	The Intel compiler tools
	The PharLap ETS compiler.

On Windows system which identify as _cygwin_:
	The MinGW tool chain
	Microsoft Visual C++ tools
	The Intel compiler tools
	The PharLap ETS compiler.

On OS/2 systems
	The OS/2 compiler
	The GCC tool chain, and the Microsoft Visual C++ tools

 On SGI IRIX, IBM AIX, Hewlett Packard HP-UX, and Oracle Solaris systems:
	The native compiler tools (MIPSpro, Visual Age, aCC, and Forte tools respectively)
	The GCC tool chain.
 
On all other platforms, including POSIX (Linux and UNIX) platforms:
	The GCC tool chain
	The Intel compiler tools

Notably missing in the windows examples is compiler selection for msys environment. I'm going to have to assume here that its going to select GCC first. 