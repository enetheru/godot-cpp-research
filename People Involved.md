I'll need to make a table of all the people interested
https://github.com/asmaloney/

https://github.com/vorlac               @Orlac

https://github.com/IvanInventor         @vano_kavo
Primary Host: Linux
Targets: All but IOS
Compilers: gcc, msvc, mingw, clang

Dragos Daian
https://github.com/Ughuuu
https://github.com/Ughuuu/godot-cpp-template/tree/add-more-stuff/options-to-build
contributes to a couple of other addons using github actions, and has contributed to godot-cpp

GDExtension team:
David Snopek (@dsnopek)                 Doesnt know cmake well enough to review
Bastiaan Olij (@BastiaanOlij)           no commented in godot-cpp for cmake, cant find significant cmake project use
Fabio Alessandrelli (@Faless)           not comments in godot-cpp for cmake, cant find significant cmake project use
George Marques (@vnen)                  no comments in godot-cpp for cmake, has a huge list of languages in his bio, cmake is not one of them
Gilles Roudière (@groud)                no comments in godot-cpp for cmake, minimal use of cmake found in personal projects
Juan Linietsky (@reduz)                 known dislike of cmake
Pāvels Nadtočajevs (@bruvzg)            no comments in godot-cpp for cmake, no mention of cmake in personal repo's

